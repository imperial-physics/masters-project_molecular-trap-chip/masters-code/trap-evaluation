(* ::Package:: *)

BeginPackage["TrapDepth`"]


TrapDepthRange::usage = ""


Begin["`Private`"]

(*Defining Constants*)
planckConstant=6.62607004*10^(-34);
BValue=10.2675*10^9; (*in Hz*)
BEnergy=BValue*planckConstant; (*In J*)
\[Mu]Value=3.06/(2.9979*10^(29)) (*3.06 Debye to SI units*)
u=1.660 539 040*10^(-27) (* in kg *)
particleCaFMass=59.076u (* in kg *)


TrapDepthRange[potentialInt_, trapCentre_]:=Module[{trapMin, trapMax, approxDepth},
trapMin=potentialInt@@trapCentre;
trapMax=Min@@Last @@@(FindPeaks[potentialInt[0,0,#]&/@Range[0,70,1]]);
approxDepth = trapMax-trapMin;
{0.95approxDepth, 1.05approxDepth}
]


End[]
EndPackage[]
