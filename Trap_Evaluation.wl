(* ::Package:: *)

SetDirectory["/home/tom/Documents/Physics_Project/trap-evaluation"];
Get["motion-solving.m"];
Get["e-field.m"];
Get["trap-depth.m"]

fieldsDir = "../field_data/Sweep36";
saveFile = "test2.csv";

particleEnergySpectrums = Get["/home/tom/Documents/Physics_Project/trap-evaluation/caf-energy-data/EnergyInterpolation"];
stateN1Spectrum = particleEnergySpectrums[[2]];
stateN2Spectrum = particleEnergySpectrums[[3]];

fieldFiles=FileNames["*.csv", fieldsDir];
splitfilenames = StringSplit[FileBaseName[#], "_"] &/@fieldFiles;
substitutions = {
"grid_mutEW",
"mutBiasSep",
"mutESep",
"mutDepth",
"voltageRatio",
"aspectratio",
"mutCurvature"
}
makeparameterstring[string_]:= string<>"_["<>string<>"]"
elements = makeparameterstring[#] &/@ substitutions
filenameParameters = StringRiffle[elements, "_"]
filenameTemplate = fieldsDir<>"/"<>filenameParameters<>".csv"
parameters = ToExpression[{#[[3]], #[[5]],#[[7]],#[[9]],#[[11]],#[[13]],#[[15]]}] &/@ splitfilenames;
sortedParameters = SortBy[parameters, #1<#2&]
CalculateTrapFrequency[curvatures_, scalefactor_] :=Sqrt[2curvatures/scalefactor]
pr[n_]:=NumberForm[n,Infinity,ExponentFunction->(Null&)]


boundaryConditions[startingPoint_]:= {x[0]==GetX[startingPoint],y[0]==GetY[startingPoint],z[0]==GetZ[startingPoint],x'[0]==GetVX[startingPoint],y'[0]==GetVY[startingPoint],z'[0]==GetVZ[startingPoint]};

terminationConditions[x_,y_,z_]:=Abs[x] >100 ||Abs[y] >100||z >100||z <0;

solveInTrap[particle_,EOM_,startingPoint_,\[CapitalDelta]t_,maxSteps_]:=Module[{startTime, endTime, path, velocities},
startTime = GetT[startingPoint];
endTime = startTime + \[CapitalDelta]t;
path =NDSolve[
{EOM}~Join~boundaryConditions[startingPoint]~Join~{WhenEvent[terminationConditions[x[t],y[t],z[t]],endTime=t;"StopIntegration"]},
{x[t],y[t], z[t]},{t,startTime,endTime},MaxSteps->maxSteps,PrecisionGoal->3,AccuracyGoal->3, Method->{(*"ImplicitRungeKutta"*)"SymplecticPartitionedRungeKutta",DifferenceOrder->4,"PositionVariables"->{x[t],y[t],z[t]}}][[1]];
velocities = D[path, t];
SetPhysicalFlag[SetTrajectory[particle,MakeTrajectory[startTime, endTime, path, velocities]],False]
];

QUntrappedAtEnergy[vType_:"Zonly",noOfParticles_,energy_,trapCentre_,timePeriod_]:=Module[{nParticles,particles,initialCloudPositions,physicalParticles,trappedParticles,untrappedParticles,minKE},
nParticles=If[vType=="ZOnly",1,noOfParticles]; (*only need one particle for ZOnly configuration*)
particles=Table[MakeParticle[],{nParticles}];
initialCloudPositions =ConstantEnergyPhaseSpacePoint[vType,energy,trapCentre] &/@ Range[nParticles];
particles=Quiet[MapThread[solveInTrap[#1,eqnOfMotion, #2,timePeriod,\[Infinity]]&,{particles,initialCloudPositions}]];
(*isolates untrapped particles*)
untrappedParticles=FindUntrappedParticles[particles,timePeriod];
(*final output is {untrapped flag, non-dimensionalised initial particle KE, dimensionalised initial particle KE}*)
{If[Length[untrappedParticles]>0,True,False],energy,energy*GetRotationalConstant[MakeParticle[]]}
];

GetTrapDepth[vType_:"ZOnly",noOfParticles_:100,timePeriod_:10000,energyRange_,energyStep_,trapCentre_]:=Module[{energyRangeMax,energy,qUntrappedAtEnergy,testEnergy,output},
TrapDepthWithError[trapDepthList_]:=Module[{trapDepthErrorList},
trapDepthErrorList={energyStep,energyStep*GetRotationalConstant[MakeParticle[]]};
(*could half energyStep for Error?*)
Riffle[trapDepthList,trapDepthErrorList]
];
(*steps through specified energy range*)
energyRangeMax=Ceiling[energyRange[[2]],energyStep];
energy=Floor[energyRange[[1]],energyStep]; (*initialises energy as start energy*)
While[((energyRangeMax-energy)/energyStep)>2,
testEnergy=(energyRangeMax+energy)/2;
Print[testEnergy];
qUntrappedAtEnergy=QUntrappedAtEnergy[vType,noOfParticles,testEnergy,trapCentre,timePeriod];
If[qUntrappedAtEnergy[[1]]==True,Print[True]; energyRangeMax=Ceiling[testEnergy,energyStep],Print[False]; energy=Floor[testEnergy,energyStep]]
];
While[energy<=energyRangeMax,
  Print[energy];
  qUntrappedAtEnergy=QUntrappedAtEnergy[vType,noOfParticles,energy,trapCentre,timePeriod];
  If[qUntrappedAtEnergy[[1]]==True,Break[]];
  energy+=energyStep
]; (*while loop breaks when \[GreaterEqual]1 particles are untrapped*)
output=If[qUntrappedAtEnergy[[1]]==True,TrapDepthWithError[Drop[qUntrappedAtEnergy,1]],{"Not Found"}];
If[output[[1]]==Floor[energyRange[[1]],energyStep],GetTrapDepthTest[vType,noOfParticles,timePeriod,energyRange-Subtract@@Reverse[trapDepthRange],energyStep,trapCentre],output]
]
(* output is: {TDND, TDerrorND, TD, TDerror} *)
];



For[i=1, i<=Length[fieldFiles],i++,
test = "["<>#<>"]" &/@substitutions;
keypairs = Transpose[{test, ToString[#]&/@sortedParameters[[i]]}];
replacementRules = #[[1]]->#[[2]] &/@ keypairs;
filename = StringReplace[filenameTemplate, replacementRules];

Print["Importing file: ", filename];
fieldvalues=ImportFieldData[filename];
(*Removes extra points from *)
zAxisPoints = fieldvalues[[Length[fieldvalues]-150;;]];
fieldvalues = fieldvalues[[1;;Length[fieldvalues]-151]];

(*Finding fitting region*)
trialZMin =GetFieldMinAlongZ[fieldvalues, 0,0,10,50];
trialMin = {0,0,trialZMin};
boundaryPoint = FindClosestPointOfBoundary[trialMin,GetBoundaryOfFittableRegion[fieldvalues, 5]];
boxsize =0.19* Norm[(boundaryPoint - trialMin)]/Sqrt[3];
fittingRegion = GetFieldRegion[fieldvalues,boxsize,boxsize,trialZMin-boxsize,trialZMin+boxsize];

(*Fitting potential*)
fittingPotential = GetxyzEnergyFieldList[fittingRegion,stateN1Spectrum];
potentialFit = NonlinearModelFit[FlattenxyzFieldList[fittingPotential],{v+a*(x-xmin)^2+b*(y-ymin)^2+c*(z-zmin)^2+d*(x-xmin)*(y-ymin)+e*(x-xmin)*(z-zmin)+f*(y-ymin)*(z-zmin),Abs[xmin]<boxsize,Abs[ymin]<boxsize,zmin>(trialZMin-boxsize),zmin<(trialZMin+boxsize)} ,{{v,2.2},{a,0.0002},{b,0.0002},{c,0.001},{d,10^-5},{e,10^-5},{f,10^-7},{xmin,0}, {ymin,0}, {zmin,trialZMin}}, {x,y,z}];
trapfitvalues = ToString[pr[#]] &/@  Riffle[Values@potentialFit["BestFitParameters"],potentialFit["ParameterErrors"]];
Print["Fit values: ", Values@potentialFit["BestFitParameters"],potentialFit["ParameterErrors"]];

(*Trap Frequencies*)
trapCurvatures = {a,b,c} /. potentialFit["BestFitParameters"];
trapFreq=CalculateTrapFrequency[trapCurvatures, GetMassParameter[MakeParticle[]]];
freqError= CalculateTrapFrequency[potentialFit["ParameterErrors"][[{2,3,4}]],GetMassParameter[MakeParticle[]]];
trapFreqPlusErr = ToString[pr[#]] &/@ Riffle[trapFreq,freqError];
Print["Freq values: ", trapFreq, freqError];

(*Trap Depth*)
energyFieldInt = ConstructEnergyInterpolation[fieldvalues, stateN1Spectrum];

trapPotential=GetEnergyAtPoint[energyFieldInt,x,y,z];
eqnOfMotion = ConstructEOM[trapPotential, {x,y,z}] /.{MotionSolving`Private`\[Gamma] ->0, MotionSolving`Private`m ->1,MotionSolving`Private`t ->t};

trapCentre={xmin, ymin, zmin} /. potentialFit["BestFitParameters"];
trapDepthRange=TrapDepthRange[energyFieldInt, trapCentre];
trapDepth = GetTrapDepth[trapDepthRange, 0.00001, trapCentre];

csvLine = {sortedParameters[[i]]~Join~trapfitvalues~Join~trapFreqPlusErr~Join~trapDepth};
Print[csvLine];
file = OpenAppend[saveFile];
Export[file, csvLine, "CSV"];
WriteString[file, "\n"];
Close[file];
]



