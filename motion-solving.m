(* ::Package:: *)

BeginPackage["MotionSolving`"]


CalculateForces::usage = "CalculateForces[trapPotential] differentiates the trap potential to find the forces acting on a particle within it"
MakeEquationOfMotion::usage = "MakeEquationOfMotion[forces] constructs equations of motion in each direction from the forces due to the trap and adds a damping term"
ConstructEOM::usage = "ConstructEOM[trapPotential] is a convenience function linking CalculateForces and MakeEquationOfMotion toGether" 

MakeParticle::usage = "MakeParticle[mass,species,trajectory] creates a placeholder particle of the given species and mass; trajectory is set to \"No trajectory set\" by default"
GetMass::usage = "GetMass[particle] returns the mass of the given particle"
GetRotationalConstant::usage = "GetRotationalConstant[particle] returns the value of the rotational constant B of the given particle in Joules"
GetDipoleMoment::usage = "GetDipoleMoment[particle_] returns the electric dipole moment of the given particle in Cm"
GetMassParameter::usage = "GetMassParameter[particle] returns the natural mass parameter m/B for the given particle"
GetSpecies::usage = "GetSpecies[particle] returns the species of the given particle"
GetPhysicalFlag::usage = "GetPhysicalFlag[particle] returns whether the particle's trajectory is given in physical units"
SetPhysicalFlag::usage = "SetPhysicalFlag[particle] sets the particle's physical flag"


GetTrajectory::usage = "GetTrajectory[particle] returns the trajectory of the given particle"
MakeTrajectory::usage = "MakeTrajectory[particle] creates a trajectory of the given particle"
SetTrajectory::usage = "SetTrajectory[particle,newTrajectory] sets the trajectory of the given particle to the new trajectory" 
PointFromTrajectory::usage = ""
PointFromParticle::usage = ""

GetStartTime::usage = "GetStartTime[trajectory]"
GetEndTime::usage = "GetStartTime[trajectory]"
GetPath::usage = "GetPath[trajectory] returns the path of the given trajectory"
GetPathFromParticle::usage ="GetPathFromParticle[particle] returns the path taken by the given particle"
GetPathValuesFromParticle::usage = "GetPathValuesFromParticle returns the path and velocities of the particle as a function of time"

MakePhaseSpacePoint::usage = "MakePhaseSpacePoint[x,y,vx,vy,t] creates a list of the given arguments to represent a phase space point"
MakePointRelativeToCentre::usage = ""
RandomPhaseSpacePoint::usage = "RandomPhaseSpacePoint[] creates a random phase space point in a unit 6D hypercube"
ConstantEnergyPhaseSpacePoint::usage = "ConstantEnergyPhaseSpacePoint[vType,energyND,trapCentre]"
GetX::usage = "GetX[point] returns the X coordinate at the given phase space point"
GetY::usage = "GetY[point] returns the Y coordinate at the given phase space point"
GetZ::usage = "GetZ[point] returns the Z coordinate at the given phase space point"
GetVX::usage = "GetVX[point] returns the velocity in the X axis at the given phase space point"
GetVY::usage = "GetVY[point] returns the velocity in the Y axis at the given phase space point"
GetVZ::usage = "GetVY[point] returns the velocity in the Z axis at the given phase space point"
GetT::usage = "GetT[point] returns the time at the given phase space point"

MakeParticlePathPhysical::usage = "Scales a particle path so that it is parametrised in terms of seconds rather than dimensionless time"
EnergyOfParticle::usage "EnergyOfParticle[particle,timeVar,time]"
EnergyOfParticleAtEndtime::usage "EnergyOfParticleAtEndtime[particle,timeVar]"
EnergyOfParticleAtEndtimeList::usage "EnergyOfParticleAtEndtimeList[particles,timeVar]"
AvgKE::usage = "AvgKE[particle,timeVar]"

FindTrappedParticles::usage = "FindTrappedParticles[particleList, endTime] finds the elements in particleList which are still in the trap after endTime"
FindUntrappedParticles::usage = "FindUntrappedParticles[particleList, endTime] finds the elements in particleList which have left the trap before endTime"
PlotMotion::usage = "PlotMotion[particle, variable, time]"
ParametricMotionPlot::usage = "ParametricMotionPlot[particle_, variable1_,variable2_, time_]"
ReturnParaPlot::usage = ""
PlotCloud::usage = "PlotCloud[cloud_,timevar_,time_,plotRange_]"
PlotPhase::usage = "PlotPhase[cloud_,variable1_,variable2_, timevar_]"


(*Calculating Properties of Trap*)
GetInitialCloudDimensions::usage = ""
GetCloudDimensions::usage = "GetCloudDimensions[cloud] returns a list of dimensions of the cloud in phase space (ie. in x,y,z,x',y',z')"

GetTrapSize::usage = "GetTrapSize[cloud] returns the volume of trappable space in \!\(\*SuperscriptBox[\(\[Mu]m\), \(3\)]\)"
GetPhaseSpaceVolume::usage = "GetPhaseSpaceVolume[cloud] returns the volume of the cloud in phase space in \!\(\*SuperscriptBox[\(\[Mu]m\), \(3\)]\)(\[Mu]m/s\!\(\*SuperscriptBox[\()\), \(3\)]\)"
CalculatePhaseAcceptance::usage = "CalculatePhaseAcceptance[trappedCloud, initialCloud] returns the phase space acceptance of the trap in \!\(\*SuperscriptBox[\(\[Mu]m\), \(3\)]\)(\!\(\*SuperscriptBox[\(ms\), \(-1\)]\)\!\(\*SuperscriptBox[\()\), \(3\)]\)"


Begin["`Private`"]

(*Defining Constants*)
planckConstant=6.62607004*10^(-34);
BValue=10.2675*10^9; (*in Hz*)
BEnergy=BValue*planckConstant; (*In J*)
\[Mu]Value=3.06/(2.9979*10^(29)) (*3.06 Debye to SI units*)
u=1.660 539 040*10^(-27) (* in kg *)
particleCaFMass=59.076u (* in kg *)


(*Building Equation of Motion*)

CalculateForces[trapPotential_, coords_]:=-D[trapPotential,#]&/@coords/.(#->#[t] &/@coords)
MakeEquationOfMotion[forces_, coords_]:=Thread[m #''[t]+\[Gamma] #'[t]&/@coords==forces]
ConstructEOM[trapPotential_, coords_] := MakeEquationOfMotion[CalculateForces[trapPotential, coords], coords]


(*Particle creation/manipulation*)

CaFProperties = {particleCaFMass, BEnergy, \[Mu]Value}
MakeParticle[properties_:CaFProperties,species_:"CaF",trajectory_:"No trajectory set"]:={properties,species, trajectory, False}
GetParticleProperties[particle_]:=particle[[1]]
GetMass[particle_]:=GetParticleProperties[particle][[1]]
GetRotationalConstant[particle_]:=GetParticleProperties[particle][[2]] (*Returns the value of B in Joules*)
GetDipoleMoment[particle_]:=GetParticleProperties[particle][[3]]
GetMassParameter[particle_]:=GetMass[particle]/(GetRotationalConstant[particle]*10^12) (*Returns m/B in units of (\[Mu]m/s)^2*)
GetSpecies[particle_]:=particle[[2]]
GetPhysicalFlag[particle_]:=particle[[4]]
SetPhysicalFlag[particle_, newFlag_]:=ReplacePart[particle,4->newFlag]
TogglePhysicalFlag[particle_]:=SetPhysicalFlag[particle,!GetPhysicalFlag[particle]]

GetTrajectory[particle_]:=particle[[3]]
MakeTrajectory[startTime_, endTime_, path_, velocity_] := {startTime, endTime, path, velocity}
SetTrajectory[particle_,newTrajectory_]:=ReplacePart[particle,3->newTrajectory]
GetStartTime[trajectory_]:=trajectory[[1]]
GetEndTime[trajectory_]:=trajectory[[2]]
GetPath[trajectory_]:=trajectory[[3]]
GetVelocity[trajectory_]:=trajectory[[4]]
GetPathValues[trajectory_]:=GetPath[trajectory]~Join~GetVelocity[trajectory]
GetPathFromParticle[particle_]:=GetPath[GetTrajectory[particle]]
GetVelocityFromParticle[particle_]:=GetVelocity[GetTrajectory[particle]]
GetPathValuesFromParticle[particle_]:=GetPathValues[GetTrajectory[particle]]

PointFromTrajectory[trajectory_,timeVar_, time_]:=Module[{positionAndVelocities, phaseSpaceParams},
positionAndVelocities=GetPathValues[trajectory]/.timeVar -> time;
phaseSpaceParams = Values@positionAndVelocities;
MakePhaseSpacePoint @@ Append[phaseSpaceParams, time]
]
PointFromParticle[particle_, timeVar_, time_]:=PointFromTrajectory[GetTrajectory[particle],timeVar, time]


(*Handling of phase space points*)

MakePhaseSpacePoint[x_,y_,z_,vx_,vy_,vz_,t_]:={x,y,z,vx,vy,vz,t}
MakePointRelativeToCentre[trapCentre_, x_,y_,z_,vx_,vy_,vz_] :=MakePhaseSpacePoint@@((trapCentre+{x,y,z})~Join~{vx,vy,vz,0} )
RandomPosition[centre_,maxDevX_,maxDevY_,maxDevZ_] :=  centre+(RandomReal[{-#,#}]&/@{maxDevX, maxDevY, maxDevZ})
RandomVelocity[maxVX_,maxVY_,maxVZ_]:= (RandomReal[{-#,#}]&/@{maxVX, maxVY, maxVZ})
RandomPhaseSpacePoint[centre_, maxDevX_,maxDevY_,maxDevZ_,maxVX_,maxVY_,maxVZ_]:= MakePhaseSpacePoint@@(RandomPosition[centre,maxDevX,maxDevY,maxDevZ] ~Join~ RandomVelocity[maxVX,maxVY,maxVZ] ~Join~ {0})

ConstantEnergyPhaseSpacePoint[vType_:"Randomised",energyND_,trapCentre_]:=
(*accepts non-dimensionalised energy*)
Module[{energy,vRef,vRefND,v0},
energy=energyND*GetRotationalConstant[MakeParticle[]]; (*dimensionalise energy*)
vRef=Abs[Sqrt[(2*energy)/GetMass[MakeParticle[]]]]; (*obtain corresponding overall velocity magnitude (vRef) from energy*)
vRefND=(10^6)*Sqrt[GetMassParameter[MakeParticle[]]]*vRef; (*non-dimensionalise vRef*)
v0 = Which[vType=="Randomised", vRefND * Normalize[RandomReal[{-1,1},3]],
vType=="XYPlane", vRefND * Normalize[RandomReal[{-1,1},2]]~Join~{0},
vType=="ZOnly", {0,0,vRefND}]; (*generate particle velocity*)
MakePointRelativeToCentre@@{trapCentre, 0,0,0}~Join~v0 (*make phase space point*)
]

GetX[point_]:=point[[1]]
GetY[point_]:=point[[2]]
GetZ[point_]:=point[[3]]
GetVX[point_]:=point[[4]]
GetVY[point_]:=point[[5]]
GetVZ[point_]:=point[[6]]
GetT[point_]:=point[[7]]


(*Dimensionalisation*)

scaleTime[rule_, scalefactor_, timevar_]:=Module[{scaledvariable},
scaledvariable = ReplacePart[rule,FirstPosition[rule,timevar]->J];
scaledvariable = ReplacePart[scaledvariable,FirstPosition[scaledvariable,timevar]->timevar/scalefactor];
scaledvariable = ReplacePart[scaledvariable,FirstPosition[scaledvariable,J]->timevar]
]

MakeParticlePathPhysical[particle_, timevar_] := Module[{scalefactor,oldTrajectory, newStart, newEnd, newPath, velocityScaling,newVelocity},
scalefactor= If[GetPhysicalFlag[particle],1/Sqrt[GetMassParameter[particle]],Sqrt[GetMassParameter[particle]]];
oldTrajectory = GetTrajectory[particle];
newStart =  GetStartTime[oldTrajectory]*scalefactor;
newEnd =  GetEndTime[oldTrajectory]*scalefactor;
(*Replaces the time in the interpolation function with the scaled dimensionful time
  Sets time symbol in the position variable to J to ensure it isn't scaled at same time*)
newPath = scaleTime[#, scalefactor, timevar] &/@GetPath[oldTrajectory];
newVelocity = scaleTime[#, scalefactor, timevar] &/@GetVelocity[oldTrajectory];
(*Need to divide by the scalefactor to account for the chain rule*)
(*factor of 10^-6 converts velocity into units of ms^-1*)
velocityScaling = 10^-6/scalefactor;
newVelocity = Thread[Rule[Keys@newVelocity,velocityScaling*Values@newVelocity]];
TogglePhysicalFlag[SetTrajectory[particle, MakeTrajectory[newStart, newEnd, newPath, newVelocity]]]
]


(*Motion plotting*)

PlotMotion[particle_, variable_, time_]:= Module[{startTime, endTime, path},
startTime =GetStartTime[GetTrajectory[particle]];
endTime = GetEndTime[GetTrajectory[particle]];
path = GetPathValuesFromParticle[particle];
Plot[variable[time] /. path,{time,startTime,endTime}]
]

ParametricMotionPlot[particle_, variable1_,variable2_, time_, plotRange1_,plotRange2_:"Not Set"]:=Module[{startTime, endTime,(* endPoint,*)path,plotRange3},
startTime =GetStartTime[GetTrajectory[particle]];
endTime = GetEndTime[GetTrajectory[particle]];
(*endPoint = PointFromParticle[particle, time, endTime];*)
path = GetPathValuesFromParticle[particle];
plotRange3=If[plotRange2=="Not Set",plotRange1,plotRange2];
Show[
	ParametricPlot[
		{variable1[time],variable2[time]}/.path,{time,startTime,If[endTime==startTime,endTime+10^-6,endTime]},AspectRatio->1,PlotPoints->200,PlotStyle->Blue
	],
	(*Graphics[{Red,PointSize[0.02],Point[{GetX[endPoint],GetY[endPoint]}]}],*)
	Graphics[{Red,PointSize[0.02],Point[{variable1[time],variable2[time]}/.path/.time->endTime]}],
	PlotRange->{{-plotRange1,plotRange1},{-plotRange3,plotRange3}}
	]
]

PlotPhase[cloud_,variable1_,variable2_, timevar_]:=
Show[
ReturnParaPlot[#,variable1,variable2,timevar]&/@cloud,AxesLabel->{ToString[variable1],ToString[variable2]}, PlotRange->Automatic, ImageSize->Medium
];

ReturnParaPlot[particle_, variable1_,variable2_, time_]:=Module[{startTime, endTime, endPoint, path},
startTime =GetStartTime[GetTrajectory[particle]];
endTime = GetEndTime[GetTrajectory[particle]];
path = GetPathValuesFromParticle[particle];
ParametricPlot[{variable1[time],variable2[time]}/.path,{time,startTime,If[endTime==startTime,endTime+10^-6,endTime]},AspectRatio->1, PlotPoints->200,PlotStyle->Blue]
]

PlotCloud[cloud_,timevar_,time_,plotRange_]:=
Show[
ListPlot[
{GetX[#],GetY[#]}&[PointFromParticle[#,timevar, time]]&/@cloud,PlotStyle->{Red,PointSize[0.015]}
],
PlotRange->{{-plotRange,plotRange},{-plotRange,plotRange}}, AxesOrigin->{0,0}
]


(*Particle Energy*)

EnergyOfParticle[particle_,timeVar_,time_]:=
0.5*GetMass[particle]*(Plus@@(Values@GetVelocityFromParticle[particle])^2/.timeVar->time)

EnergyOfParticleAtEndtime[particle_,timeVar_]:=Module[{endTime},
endTime = GetEndTime[GetTrajectory[particle]];
EnergyOfParticle[particle,timeVar,endTime]
]

EnergyOfParticleAtEndtimeList[particles_,timeVar_]:=EnergyOfParticleAtEndtime[#,timeVar]&/@particles

AvgKE[particle_, timeVar_]:=Module[{startTime,endTime,path,velocityVals},
startTime =GetStartTime[GetTrajectory[particle]];
endTime = GetEndTime[GetTrajectory[particle]];
path = GetPathValuesFromParticle[particle];
(*Integrate and divide by total time to Get average KE*)
1/(endTime-startTime) * NIntegrate[EnergyOfParticle[particle,timeVar,t],{t,startTime,endTime}]
]


(*Finding Trapped & Untrapped particles*)
FindTrappedParticles[particleList_, endTime_]:=Select[particleList, GetEndTime[GetTrajectory[#]]>=endTime&]

FindUntrappedParticles[particleList_, endTime_]:=Select[particleList, GetEndTime[GetTrajectory[#]]<endTime&]



SampleInterpolationFunction[intFunc_]:=intFunc /. x_InterpolatingFunction[t_] -> x["ValuesOnGrid"]


(*Calculating Properties of Trap*)

GetInitialCloudDimensions[cloud_]:=Module[{variables},
variables = Keys@GetPathValuesFromParticle[First[cloud]];
GetStartPointofInterpolation[#, cloud] &/@ variables
]
GetStartPointofInterpolation[variable_, cloud_]:=Max[GetStartPoint[(variable/.GetPathValuesFromParticle[#])] &/@ cloud] - Min[GetStartPoint[(variable/.GetPathValuesFromParticle[#])] &/@ cloud]
GetStartPoint[intFunc_]:=intFunc /. x_InterpolatingFunction[t_] -> x[0]

GetCloudDimensions[cloud_]:=Module[{variables},
variables = Keys@GetPathValuesFromParticle[First[cloud]];
GetMinMaxofInterpolation[#, cloud] &/@ variables
]
(*Helper functions for GetCloudDimensions*)
GetMinMaxofInterpolation[variable_, cloud_]:=Max[SampleInterpolationFunction[(variable/.GetPathValuesFromParticle[#])] &/@ cloud] - Min[SampleInterpolationFunction[(variable/.GetPathValuesFromParticle[#])] &/@ cloud]
SampleInterpolationFunction[intFunc_]:=intFunc /. x_InterpolatingFunction[t_] -> x["ValuesOnGrid"]

GetTrapSize[cloud_]:= Times@@GetCloudDimensions[cloud][[1;;3]]
GetPhaseSpaceVolume[cloud_] := Times@@ GetCloudDimensions[cloud]

CalculatePhaseAcceptance[trappedCloud_, initialCloud_]:=Length[trappedCloud]/Length[initialCloud] * (Times@@GetInitialCloudDimensions[initialCloud])


End[]
EndPackage[]
