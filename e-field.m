(* ::Package:: *)

BeginPackage["eField`"]

ImportFieldData::usage = "importFieldData[fileName] imports the electric field data from a given text file exported from COMSOL"
GetFieldRegion::usage ="GetFieldRegion[fieldValues,xMax,yMax,zMin,zMax] returns a region of a given field within certain coordinates"

SphericalEField3D::usage = "SphericalEField3D[xMax,yMax,zMax,res] creates a list of 2D xy slices (square matrices) of a spherical 3D E-field with minimum at the centre"
CubeEField3D::usage = "CubeEField3D[xMax,yMax,zMax,res] creates a list of 2D xy slices (square matrices) of a cube shaped 3D E-field, uniform minimum cube E-field at the centre surrounded by a constant maximum"
TransformEFieldTensor::usage = "TransformEFieldTensor[eFieldTensor,xMax,yMax,zMax,res] transforms eFieldTensor into a list of {{x,y,z},eFieldValue} lists"

GetBoundaryOfFittableRegion::usage = ""
FindClosestPointOfBoundary::usage = ""
GetFieldMinAlongZ::usage = ""

NonDimensionaliseField::usage = "NonDimensionaliseField[xyzEFieldList,B,\[Mu]] returns non dimensionalise version of xyzEFieldList; B and \[Mu] are set to values for CaF by default"
GetxyzEnergyFieldList::usage = "GetxyzEnergyFieldList[xyzEFieldList,energyInterpolation] returns a list of {{x,y,z},energyFieldValue} lists"
GetEnergyFieldInterpolation::usage = "GetEnergyFieldInterpolation[xyzEnergyFieldList] returns the interpolation function for the energy field given a list of {{x,y,z},energyFieldValue} lists"
ConstructEnergyInterpolation::usage = "ConstructEnergyInterpolation[xyzEFieldList,energyInterpolation] returns the interpolation function for the energy field given a list of {{x,y,z},eFieldValue} lists"
GetEnergyAtPoint::usage = "GetEnergyAtPoint[energyFieldInt,x,y,z] returns the energy at the point {x,y,z}"
FlattenxyzFieldList::usage = "FlattenxyzFieldList[xyzEFieldList] transforms list of {{x,y,z},eFieldValue} lists -> list of {x,y,z,eFieldValue} lists"
PlotEField::usage = "PlotEField[xyzEFieldList,Options[PlotType,ContourParam]] plots the 3D E-field given a list of {{x,y,z},eFieldValue} lists. PlotType option values: {\"Density\",\"Contour\"}, default is \"Density\". Plotrange option allows user to set range for plot, default is All. ContourParam option allows user to customise contours for the contour plot, default is Automatic"
PlotEnergyField::usage = "PlotEField[xyzEnergyFieldList,Options[PlotType,ContourParam]] plots the 3D energy field given a list of {{x,y,z},energyFieldValue} lists. PlotType option values: {\"Slices\",\"Density\",\"Contour\"}, default is \"Slices\". Plotrange option allows user to set range for plot, default is All. ContourParam option allows user to customise contours for the contour plot, default is Automatic"


Begin["`Private`"]

(*Defining Constants*)
sweetSpotEfield=2.0314830292763326*10^6; (*in V/m*)
planckConstant=6.62607004*10^(-34);
BValue=10.2675*10^9; (*in Hz*)
BEnergy=BValue*planckConstant; (*In J*)
\[Mu]Value=3.06/(2.9979*10^(29)) (*3.06 Debye to SI units*)


(*Importing Field Data*)
Groupcoord[line_] := {line[[1;;3]], line[[4]]}

ImportFieldData[fileName_]:=Module[{fieldValues},
fieldValues = Import[fileName] /."nan"->0;
fieldValues = Map[Groupcoord,fieldValues,{1}];
fieldValues
]

(*Snips a region of a field*)
GetFieldRegion[fieldValues_,xMax_, yMax_, zMin_, zMax_]:=Module[{},
Select[fieldValues, Abs[#[[1,1]]] <xMax && Abs[#[[1,2]]]< yMax &&Abs[#[[1,3]]]>zMin && Abs[#[[1,3]]]<zMax&] 
]


(*Functions for Simulated Field*)
SphericalEField3D[xMax_,yMax_,zMax_,res_,width_]:=sweetSpotEfield*Table[Exp[((x/width)^2+(y/width)^2+(z/width)^2)],
{z,-zMax,zMax,res},{y,-yMax,yMax,res},{x,-xMax,xMax,res}]

TransformEFieldTensor[eFieldTensor_,xMax_,yMax_,zMax_,res_]:=Module[{dimZYX,indexList,xyzList,xList,yList,zList},
dimZYX=Dimensions[eFieldTensor]; (* returns as {dimz,dimy,dimx} *)
indexList=Flatten[Table[{x,y,z},{z,1,dimZYX[[1]]},{y,1,dimZYX[[2]]},{x,1,dimZYX[[3]]}],2]; (*produces a list of lists of {i1,i2,i3}, where i1,i2,i3 are indices*)
xyzList=Table[i,{i,-#,#,res}]&/@{xMax,yMax,zMax};
xList=xyzList[[1]];
yList=xyzList[[2]];
zList=xyzList[[3]];
{{xList[[#[[1]]]],yList[[#[[2]]]],zList[[#[[3]]]]},eFieldTensor[[#[[3]]]][[#[[2]]]][[#[[1]]]]}&/@indexList
]


(*Preparing fields for fitting*)

GetBoundaryOfFittableRegion[fieldValues_, breakdownField_]:=Module[{NDField},
NDField =NonDimensionaliseField[fieldValues];
(*Collect points at which ND field is above the breakdown field strength as given by Stark shift*)
#[[1]] &/@Select[NDField, #[[2]]>=breakdownField &]
]

FindClosestPointOfBoundary[point_, boundaryPoints_]:=Module[{vectorsToBoundary, distances, minDistance},
vectorsToBoundary =Plus [point,#]&/@boundaryPoints;
distances = Norm[#]&/@ vectorsToBoundary;
minDistance  = Min[distances];
(*Finds index of closest boundary point*)
nearestPoint=Ordering[distances,1];
(*Returns closest boundary point*)
boundaryPoints[[nearestPoint]][[1]]
]

GetFieldMinAlongZ[fieldValues_,xCentre_:0,yCentre_:0,zMin_:0,zMax_:50]:=Module[{eFieldInt},
eFieldInt=Interpolation[fieldValues];
(Values@NMinimize[{eFieldInt[xCentre,yCentre,z],zMin<z<zMax},z][[2]])[[1]]
]


(*Creating Interpolation Functions*)
NonDimensionaliseField[xyzEFieldList_,B_:BEnergy,\[Mu]_:\[Mu]Value]:=ReplacePart[#, 2->(\[Mu]/B)*#[[2]]] &/@xyzEFieldList
(*
B=BEnergy (* in units of energy kgm^2s^-2 *)
\[Mu]=\[Mu]Value (*in units of Cm where 1C=1As^(-1) *)
		 (* 1Cm = 2.9979*10^29D *)
*)

GetxyzEnergyFieldList[xyzEFieldList_,energyInterpolation_]:=Module[{xyzEFieldListND, \[Lambda]},
xyzEFieldListND=NonDimensionaliseField[xyzEFieldList];
ReplacePart[#, 2->(energyInterpolation[\[Lambda]]/.\[Lambda]->#[[2]])] &/@ xyzEFieldListND
]

GetEnergyFieldInterpolation[xyzEnergyFieldList_]:=Interpolation[xyzEnergyFieldList]

ConstructEnergyInterpolation[xyzEFieldList_,energyInterpolation_]:= Module[{xyzEnergyFieldList},
xyzEnergyFieldList=GetxyzEnergyFieldList[xyzEFieldList,energyInterpolation];
GetEnergyFieldInterpolation[xyzEnergyFieldList]
]

GetEnergyAtPoint[energyFieldInt_,x_,y_,z_]:=energyFieldInt[x,y,z]
(*GetEnergyAtPoint[energyFieldInt_,x_,y_,z_]:=DimensionaliseEnergy[energyFieldInt[x,y,z]]*)


(*Plotting Fields*)
FlattenxyzFieldList[xyzFieldList_]:=Flatten[#]&/@xyzFieldList

PlotEField[xyzEFieldList_,OptionsPattern[]]:=Module[{contournumber},	
contournumber=If[OptionValue[ContourNumber]=="Automatic",Automatic,OptionValue[ContourNumber]];
Which[OptionValue[PlotType]=="Density",
ListDensityPlot3D[FlattenxyzFieldList[xyzEFieldList],ColorFunction->"Rainbow",
PlotLegends->Automatic,AxesLabel->{"x","y","z"},TargetUnits->{"\[Mu]m","\[Mu]m","\[Mu]m","V/m"}],
OptionValue[PlotType]=="Contour",
ListContourPlot3D[FlattenxyzFieldList[xyzEFieldList],OptionValue[ContourParam],Mesh->None,
PlotLegends->Automatic,AxesLabel->{"x","y","z"},TargetUnits->{"\[Mu]m","\[Mu]m","\[Mu]m","V/m"},ContourStyle->Opacity[0.5]]]
]
Options[PlotEField]={PlotType->"Density",ContourParam->Automatic};

(* Plotting Energy Fields *)
Plot2DEnergyFieldSlice[xyzEnergyFieldList_,xmin_:0,ymin_:0,zmin_:0]:=
Module[{xyzVals,xMinMax,yMinMax,zMinMax,energyFieldInt},
xyzVals=Transpose[#[[1]]&/@xyzEnergyFieldList];
xMinMax=MinMax[xyzVals[[1]]];
yMinMax=MinMax[xyzVals[[2]]];
zMinMax=MinMax[xyzVals[[3]]];
energyFieldInt = GetEnergyFieldInterpolation[xyzEnergyFieldList];
{Plot[energyFieldInt[x,ymin,zmin],{x,xMinMax[[1]],xMinMax[[2]]},
PlotLabel->"Potential in y-z plane",AxesLabel->{"x","J/B"}, ImageSize->Medium],
Plot[energyFieldInt[xmin,y,zmin],{y,yMinMax[[1]],yMinMax[[2]]},
PlotLabel->"Potential in x-z plane",AxesLabel->{"y","J/B"}, ImageSize->Medium],
Plot[energyFieldInt[xmin,ymin,z],{z,zMinMax[[1]],zMinMax[[2]]},
PlotLabel->"Potential in x-y plane",AxesLabel->{"z","J/B"}, ImageSize->Medium]}
]

PlotEnergyField[xyzEnergyFieldList_,OptionsPattern[]]:=
Module[{axesLabel,targetUnits,trapCentre},
axesLabel={"x","y","z"};
targetUnits={"\[Mu]m","\[Mu]m","\[Mu]m","J/B"};
trapCentre=OptionValue[TrapCentre];
Which[OptionValue[PlotType]=="Slices",
Plot2DEnergyFieldSlice[xyzEnergyFieldList,trapCentre[[1]],trapCentre[[2]],trapCentre[[3]]],
OptionValue[PlotType]=="Density",
ListDensityPlot3D[FlattenxyzFieldList[xyzEnergyFieldList],ColorFunction->"Rainbow",
PlotLegends->Automatic,AxesLabel->axesLabel,TargetUnits->targetUnits],
OptionValue[PlotType]=="Contour",
ListContourPlot3D[FlattenxyzFieldList[xyzEnergyFieldList],Contours->OptionValue[ContourParam],
Mesh->None,PlotLegends->Automatic,AxesLabel->axesLabel,TargetUnits->targetUnits,
ContourStyle->Opacity[0.5]]]
]
Options[PlotEnergyField]={PlotType->"Slices",TrapCentre->{0,0,0},ContourParam->10};


End[]
EndPackage[]
